
# Master Test Plan
>### Social Network
>Prepared by: *Aneliya Nestorova and Atanas Dimitrov* <br>
>Version: 2<br>
>Date: 30.11.2019

## Table of contents

1. **[TEST PLAN IDENTIFIER](#header_test_plan_identifier)**
2. **[REFERENCES](#header_references)**
3. **[INTRODUCTION](#header_introduction)**
4. **[OBJECTIVES](#header_objectives)**
5. **[TASKS](#header_tasks)**
6. **[SCOPE](#header_scope)**
7. **[TESTING STRATEGY](#header_testing_strategy)**<br>
7.1. **[UNIT TESTING](#header_unit_testing)**<BR>
7.2. **[INTEGRATION TESTING](#header_integration_testing)**<br>
7.3. **[SYSTEM TESTING](#header_system_testing)**<br>
7.4. **[USER ACCEPTANCE TESTING](#header_user_acceptance_testing)**<br>
7.5. **[AUTOMATED REGRESSION TESTS](#header_automated_regression_tests)**
8. **[HARDWARE REQUIREMENTS](#header_hardware_requirements)**
9. **[ENVIRONMENT NEEDS](#header_environment_needs)**<br>
9.1. **[MAIN FRAME](#header_main_frame)**<br>
9.2. **[WORKSTATION](#header_workstation)**
10. **[RISK/ASSUMPTIONS](#header_risk_assumptions)**
11. **[FEATURES TO BE TESTED](#header_features_to_be_tested)**
12. **[FEATURES NOT TO BE TESTED](#header_out_of_scope)**
13. **[PROCESS OVERVIEW](#header_process_overview)**
14. **[TESTING PROCESS](#header_testing_process)**
15. **[PASS/FAIL CRITERIA](#header_pass_fail_criteria)**
16. **[SUSPENSION CRITERIA](#header_suspension_resumption_criteria)**
17. **[TEST DELIVERABLES](#header_deliverables)**
18. **[RESPONSIBILITIES](#header_responsibilities)**
19. **[SCHEDULE](#header_schedule)**
20. **[APPENDICIES](#header_appendicies)**
21. **[APPROVALS](#header_approvals)**



### 1. <a name="header_test_plan_identifier"></a>**TEST PLAN IDENTIFIER**
This is the test plan for the Social Network Application, assigned to the Java Cohort in Telerik Academy Alpha Program. The plan will ensure the functionality of the application. This Test Plan reference is TSN.
### 2. <a name="header_references"></a>**REFERENCES**
This Test Plan is based on the following documents:
- 	Java Team Project Assignment;
- 	Scale Focus QA Team Project Assignment;
- 	International standard testing;

### 3. <a name="header_introduction"></a>**INTRODUCTION**
This Test plan is intended to give a complete planning of a strategy for software testing of the Social network Application, assigned to the Java Cohort at Telerik Academy Alpha program. The Social network application is composed of several features  - Registration, Login, Profiles, Posts, Comments and News Feed, as well as Administration module. The test plan will ensure, those features work and form a complete application.

### 4. <a name="header_objectives"></a>**OBJECTIVES**
This Test plan supports the following objectives:
-	Identify existing project information
-	Identify the approach that should be followed
-	Identify the features, that should be tested
-	List the recommended test requirements
-	Describe testing strategy
-	Identify the required test resources and provide an estimation of the test efforts
-	Create schedule of the testing activities
-	Identify the risk associated with the test strategy
-	List the deliverable elements of the test activities



### 5. <a name="header_tasks"></a>**TASKS**

The Tasks will include

- 	Test cases design
-	Testing
-	Post-testing
-	Reporting


### 6. <a name="header_scope"></a>**SCOPE**

Testing will begin at the component level and will move toward the integration of the entire system. This plan will validate the functionality of the Social Network Application against the requirements. So the whole specification will be strictly followed in every step. As the application has a numerous set of features, we will prioritized the features and testing will take place according to this priority.

### 7. <a name="header_testing_strategy"></a>**TESTING STRATEGY**
	
The testing will be done manually, until the application is sufficiently stable to begin automation tests. The testing will cover the requirements for all of the different roles and features.<br> <br>

#### 7.1. <a name="header_unit_testing"></a>**UNIT TESTING**

- **Definition**<br>
The unit testing will be done at a source code level of the application. This includes language specific programming errors, such as bad syntax, logic errors, testing of particular functions or code modules. The following techniques will be used:
- White box testing<br>
The UI is bypassed and the inputs and outputs are tested directly at the code level and the results are compared with the specifications.
- Black box testing<br>
Typically involves checking every possible input to verify that it results in the right outputs, using the application as an end-user would.
- **Participants**<br>
The unit tests will be done by the developers’ team.
- **Methodology**<br>
The main methods for the black box testing will be Equivalence Partitioning and Boundary value analysis
- Equivalence Partitioning<br>
For this testing methods two main cases will be used.<br> 
	Valid input – Test values within boundaries of the specified equivalence classes. This shall be values which the application expects and transforms into usable output.<br>
	Invalid input – Test values outside the boundaries given in the specification. The application should not produce meaningful output, even if accepts the input.
Using both valid and invalid inputs, the developers will create test cases.
- Boundary value analysis<br>
The acceptable range of values will be defined by the developers’ team. With this values they will generate test cases, performing boundary value analysis.
#### 7.2. <a name="header_integration_testing"></a>**INTEGRATION TESTING**

- **Definition**<br>
Integration tests exercise the entire system and ensures that a set of components play nicely together.
- **Participants**<br>
The integration tests will be performed by the QA Team.
- **Methodology**<br>
We will utilize the Jenkins server to run the integration testing automatically whenever there is a Pull Request created on Gitlab and when the testing has finished, Jenkins will report the result to Gitlab. Therefore, we only needs to evaluate the result from Jenkins. If everything is good then changes can be accepted and merged to the master branch.
#### 7.3. <a name="header_system_testing"></a>**SYSTEM TESTING**

- **Definition**<br>
The goal of the system testing is to detect faults in the entire integrated system and to check how components interact with one another and with the system as a whole. This is also called End to End testing scenario. Generally system testing is mainly concerned in areas as functional, performance, security, load, volume, endurance and so on. In our case we will be focused on functional testing.
- **Participants**<br>
The integration tests will be performed by the QA Team.
- **Methodology**<br>
End to End testing scenarios will be tested manually and automatically. For automated tests we will use Selenium Web Driver with Test NG and Postman. We have also used Jenkins for running the automated tests, to ensure the quality of the testing process. If a job failed, an e-mail was send to the team.
#### 7.4. <a name="header_user_acceptance_testing"></a>**USER ACCEPTANCE TESTING**

- **Definition**<br>
The purpose of acceptance test is to confirm that the system is ready for operational use. During acceptance test, end-users (customers) of the system compare the system to its initial requirements.
- **Participants**<br>
User acceptance testing is out of the scope of testing for this application.

#### 7.5. <a name="header_automated_regression_tests"></a>**AUTOMATED REGRESSION TESTS**

- **Definition**<br>
Regression testing involves testing, done to make sure none of the changes made over the course of the development process have caused new bugs. It also makes sure no old bugs appear from the addition of new software modules over time.
- **Participants**<br>
Out of Scope


### 8. <a name="header_hardware_requirements"></a>**HARDWARE REQUIREMENTS**

- **Computers** :
    - Laptop Dell Latitude-E6440 | OS Ubuntu 64bit
    - Laptop Asus | Windows 10 pro 64bit

- **Servers** :
    - Localhost
    - Dell Precision T5500 12GB DDR3; Intel Xeon Six-Core E5649 12MB L3 Cache) 


### 9. <a name="header_environment_needs"></a>**ENVIRONMENT NEEDS**
#### 9.1. <a name="header_main_frame"></a>**MAIN FRAME**

A testing environment is a setup of software and hardware for the testing teams to execute test cases. In other words, it supports test execution with hardware, software and network configured.<br>

For the test environment, a key area to set up includes:

**System and applications**

We used a computer Dell Precision T5500 12GB DDR3; Intel Xeon Six-Core E5649 12MB L3 Cache) as Server<br>

**Test data**

As an application was developed to work on localhost, we have an empty database, which we fill with non-sensitive data<br>
**Database server**

Database server is MySQL/Maria DB<br>

**Front-end running environment**

Computers as stated in point 8<br>

**Client operating system**

Unknown<br>

**Browser**

Chrome and Firefox - version ... <br>

**Documentation required:** 

Document for setting the database and running the application from the developers team<br>
Slagger for REST API Testing from the developers team 
#### 9.2. <a name="header_workstation"></a>**WORKSTATION**
- **Computers** :
    - Laptop Dell Latitude-E6440 | OS Ubuntu 64bit
    - Laptop Asus | Windows 10 pro 64bit

- **Servers** :
    - Localhost
    - Dell Precision T5500 12GB DDR3; Intel Xeon Six-Core E5649 12MB L3 Cache) 

### 10. <a name="header_risk_assumptions"></a>**RISK/ASSUMPTION**
Our major goal is to design a test strategy, which cover a representative sample of the system, in order to minimize the risk. Speaking about the risk, two major components have to be taken into account:
 - The probability that the negative event will occur
 - The potential loss or impact associated with the event.<br>

**The main risks we identified are as follows:**<br>
- **Not enough training/Lack of competence** – As the application will be tested from students from Telerik Academy with no previous experience, there is a possibility of misunderstanding or misapplication of the testing techniques.
 - **Us vs. Them Mentality** – This common problem arises, when Developers and QA team are on the opposite sides of the test issue. As the developers and QA have lack of experience, they may arise this kind of situation.
 - **Lack of User involvement** – Users play a significant role in testing, as they assure that the software works from their perspective. It will be really hard to involve Users in the testing process.
 - **Rapid change of the system** – There are continuously coming changes in the application, which may affect the testing process.
### 11. <a name="header_features_to_be_tested"></a>**FEATURES TO BE TESTED**
| **Feature** | **Priority** | **Desription**|
|-------------|--------------|---------------|
|**Public part**||
|Registration|	1	|Create User 
Login|	1	|Access created User profile|
Logout|	1	|Log out from the system|
Appropriate Error Message Processing	|1|	It is importantant for admin as well as User (optional)|
**Private part ( Registered users only)** ||
Change name	|1|	Change profile information|
Upload a profile picture|	1	|Change profile information|
Set visibility of the picture	|1|	Change profile information|
Change password|	2|	Change profile information(optional)|
Change e-mail|	2|	Change profile information(optional)||
Profile Search|	1	|Search for users
Change user personal info|	2|	Change profile information(optional)|
View porfile|	1	|Registered user can view the profile of another registered user|
Request a connection |	1|	Registered user can request a connection with another user|
Approve connection request|	1|	The connected user has to approve the request of the requested user|
Request to disconnect	|1|	Registered user can disconect a connection with another user|
Create post|	1	|A registered user can create post|
Set post to public/private|	1|	A registered user can set the post privacy|
Post content	|1|	Validate that post contains text|
Post content|	2	|Validate that post contains picture (optional)|
Chronological orders of posts	|1|	Check if the posts are ordered chronologically|
Like/unlike the post	|1|	A registered user can like/unlike posts|
Count likes|	1|	Show numbers of likes under the post|
Comment post|	1|	Registered users can comment on the post|
Limit comments|	1|	Set a comment limit, which can be visible along the post content|
Show comments	|1|	Show exactly the number of limited comments along the post content|
**Administration part**||
Change profile|	1|	Delete profile|
Change post	|1|	Delete post|
Change comments|	1|	Delete comment|

### 12. <a name="header_features_not_to_be_tested"></a>**FEATURES NOT TO BE TESTED**
| **Feature** | **Priority** | **Desription**|
|-------------|--------------|---------------|
|**Public part**|||
Email confirmation|	2	|lack of time||
Forgotten password|	2	|Feature does not exist|
Public Feed|	1|	Feature does not exists||
Profile Search|	1	|Feature does not exists
Show Registered Users|	1	|Feature does not exist|
Show Profile picture|	1|	Feature does not exists|
Show posts ordered chronologically|	1	|Feature does not exists|
**Private part ( Registered users only)** ||
Set visibility of the picture	|1|	The feature does not exists|
**Administration part**||
Edit profile|	1|	The feature does not exists|
Edit post	|1|	The feature does not exists|
Edit comments|	1|The feature does not exists|

### 13. <a name="header_process_overview"></a>**PROCESS OVERVIEW**
The following represents the overall flow of the testing process
1.	Identify the requirements to be tested. All test cases should be derived in accordance with the specifications.
2.	Identify which particular test will be used to test each module.
3.	Review the test data and the test cases to be sure that the unit has been verified and the test data and the test cases are adequate to verify the unit properly.
4.	Identify the expected results for each test.
5.	Document the test case configuration, test data, and expected results.
6.	Perform the tests.
7.	Document the test data, test cases, and test configuration, used during the testing process. This information should be submitted via Test Report.
8.	Successful unit testing is eligible prior integration/system testing.
9.	Unsuccessful testing requires a Bug Report to be generated. The Bug Report should include the title, the problem encountered and steps to reproduce the bug.
10.	 Test documentation and reports should be submitted. Any specifications to be reviewed, revised or updated, should be handled immediately.

### 14. <a name="header_testing_process"></a>**TESTING PROCESS**
![Testing process](test-management-process.png)

### 15. <a name="header_pass_fail_criteria"></a>**PASS/FAIL CRITERIA**
1.	According to given scenario, the expected results need to take place, than the criteria will be considered as passed, otherwise that criteria will fail.
3.	System crash will be considered as fail case.
4.	After submitting a query in the system, if expected page won’t appear, it will be considered as fail case. 

### 16. <a name="header_suspension_resumption_criteria"></a>**SUSPENSION CRITERIA**
Testing will be performed to ensure, that all features work well in the system. Given below are some criteria, for which we will paused the test work.
1.	If smoke test failed.
2.	Certain individual test cases can be skipped, suspended or reduced, if prerequisite tests have previously failed, e.g. usability testing may be skipped, if significant number of web application navigational tests failed.

### 17. <a name="header_deliverables"></a>**TEST DELIVERABLES**
1.	Test plan ( this document)
2.	Test cases and Rest API tests
5.	Script for execution of the automation tests.
6.  A document, with required prerequicites to run the tests
7.  A document with a short description how to run the tests, how to filter them, etc.
7.	Test reports
8.	Bug reports
9.  Exploratory session reports


### 18. <a name="header_responsibilities"></a>**RESPONSIBILITIES**
| **Role** | **Responcibilities** | **Timing** |
|----------|-------------------|------------|
|QA Team	|Create Test plan;<br> Identify/ test data;<br> Execute test conditions;<br> Produce expected results;<br> Designing test cases;<br> Creating the test procedure;<br> Executing tests;<br> Preparing reports	<br>|Start to end of the project|
Developers' team|	Ensure Unit testing;<br> Review each modul before merging<br>|	Development time|


### 19. <a name="header_schedule"></a>**SCHEDULE**

| **Test Phase** | **Duration<br>(days)** 
|-----------|------------
| Test plan ( this document) | 3
| High level test cases | 5
| Rest API tests | 10
| UI tests | 10
| Script for running the automation tests | 1 
| Test reports | 1
| Bug reports | 1
| Manual testing | 3
| Re-testing | 3
| Presentation | 1
### 20. <a name="header_appendicies"></a>**APPENDICIES**
1.	Test case template
2.	Test report template
3.	Bug report template

### 21. <a name="header_approvals"></a>**APPROVALS**

| **Name (In Capital Letters)** | **Signature** | **Date** |
|---------------------------|-----------|------|
| 1. Aneliya Nestorova| | 30/11/19 |
| 2. Atanas Dimitrov | | 30/11/19 |

