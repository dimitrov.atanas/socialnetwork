### How to RUN postman test suite

1. First you will need to install:
 - newman
 - newman-reporter-htmlextra

2. Run the collections<br>
2.1 For Windows<br> 
 - run runFailersCollection.bat file
 - run runUpdateEmailAddCommentCollection.bat file<br>
2.2 For Ubuntu<br>
 - run apiFailures
 - run apiFailuresLocal
 - run apiUpdateEmail
 - run apiUpdateEmailLocal


### How to install newman & newman-reporter-htmlextra

For newman if you want to install globally use the following command
>$ npm install -g newman

If you want to install locally you need to remove the -g option as follow:
>$ npm install newman


For the newman-reporter-htmlextra you need to run the following command if:
- newman is install globally
>npm install -g newman-reporter-htmlextra
- newman is install locally
>npm install -S newman-reporter-htmlextra

Output of the test suite should be in the following directory
>newman


### Files needed in order to everything go smoothly

1. collections
>failures.postman_collection.json
>updateEmail_AddComment.postman_collection.json

2. environment
>social-network.postman_environment_LOCAL.json
>social-network.postman_environment_SERVER.json

3. Double click the corresponding file or run it trought terminal/command prompt<br>
>This will run the test suite and will generate HTML report.
