package tests;

import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pages.enums.PostStatus;

import java.io.File;

public class CreateNewPostTest extends BaseTest {

    @Test(priority = 1, description = "TC007 - Successful login")
    @Description("Test Description: Verify if a user will be able to login with a valid credentials")
    public void login()  {

        homePage.goToHomePage()
                .goToLoginPage();

        loginPage
                .loginToIllumni("testuser1", "testpass1")
                .goToProfilePage();

    }
        @Test(priority = 2, description = "TC017 - Create post")
        @Description("Test Description: Verify that user can create post")
        public void createNewPost() throws InterruptedException {

            String filePath = new File("src/test/resources/img/2.jpg").getAbsolutePath();


            profilePage
                    .verifyProfilePageTitle("Profile")
                    .createNewPost(PostStatus.PUBLIC, "world Of Warcraft", "first description ", filePath);

            profilePage.verifyNewPostAppears("diablo2");



    }

}
