package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.testng.annotations.*;
import pages.*;
import utilities.Log;

import java.util.concurrent.TimeUnit;


public class BaseTest {
    protected WebDriver driver;
    HomePage homePage;
    LoginPage loginPage;
    ProfilePage profilePage;
    NewsFeed newsFeed;
    RegistrationPage registrationPage;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeClass
    public void classSetUp() {
        //Log when Test is starting
        Log.startLog("Test is starting");

        if (driver == null) {

            if ((System.getenv("driver") == null)) {

                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();

                System.out.println("[INFO] Default driver is chrome");

                if (System.getenv("headless") != null) {
                    options.addArguments("--headless", "--incognito", "--disable-gpu", "--window-size=1920,1200");
                    driver = new ChromeDriver(options);
                    System.out.println("[INFO] Web Driver is headless");

                } else {
                    options.addArguments("--incognito");
                    driver = new ChromeDriver(options);
                }


            } else if (System.getenv("driver").equals("chrome")) {

                ChromeOptions options = new ChromeOptions();
                WebDriverManager.chromedriver().setup();

                System.out.println("[INFO] driver is chrome");

                if (System.getenv("headless") != null) {

                    options.addArguments("--headless", "--incognito", "--disable-gpu", "--window-size=1920,1200");
                    driver = new ChromeDriver(options);
                    System.out.println("[INFO] Web Driver is headless");

                } else {
                    options.addArguments("--incognito");
                    driver = new ChromeDriver(options);
                }


            } else if (System.getenv("driver").equals("firefox")) {

                WebDriverManager.firefoxdriver().setup();

                System.out.println("[INFO] driver is firefox");
                FirefoxOptions options = new FirefoxOptions();
                if (System.getenv("headless") != null) {
                    options.addArguments("--headless", "--incognito", "--disable-gpu", "--window-size=1920,1200");
                    driver = new FirefoxDriver(options);
                    System.out.println("[INFO] Web Driver is headless");

                } else {
                    options.addArguments("--incognito");
                    driver = new FirefoxDriver(options);
                }

            } else if (System.getenv("driver").equals("opera")) {
                System.out.println("[INFO] driver is opera");
                WebDriverManager.operadriver().setup();

                OperaOptions options = new OperaOptions();
                if (System.getenv("headless") != null) {
                    options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200");
                    driver = new OperaDriver(options);
                    System.out.println("[INFO] Web Driver is headless");

                } else {
                    options.addArguments("--incognito");
                    driver = new OperaDriver(options);
                }

            } else {
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();

                System.out.println("[INFO] The driver was not set properly!");
                System.out.println("[INFO] Default driver is chrome");
                System.out.println("[INFO] If You want to change it to firefox, set driver=firefox.");

                if (System.getenv("headless") != null) {
                    options.addArguments("--headless", "--incognito", "--disable-gpu", "--window-size=1920,1200");
                    driver = new ChromeDriver(options);
                    System.out.println("[INFO] Web Driver is headless");

                } else {
                    options.addArguments("--incognito");
                    driver = new ChromeDriver(options);
                }
            }
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }



    }

    @BeforeMethod
    public void methodSetup() {
        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
        profilePage = new ProfilePage(driver);
        newsFeed = new NewsFeed(driver);
        registrationPage = new RegistrationPage(driver);
    }


    @AfterClass
    public void tearDown() {
        //Log when Test is ending
        Log.endLog("Test is ending");
        driver.quit();
    }

}
