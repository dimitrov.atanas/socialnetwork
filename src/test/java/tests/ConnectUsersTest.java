package tests;

import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pages.enums.PostStatus;
import utils.Listeners.Retry;

import java.io.File;

public class ConnectUsersTest extends BaseTest {

    @Test(priority = 1, description = "TC001 - Successful registration")
    @Description("Test Description: Verify if a user will be able to register with a valid username and valid password")
    public void registerRandomUser() {

        homePage.goToHomePage()
                .verifyHomePageTitle("Home")
                .goToRegistrationPage();

        registrationPage
                .verifyRegistrationPageTitle("Registration")
                .registerNewRandomUser();

    }

    @Test(priority = 2, description = "TC007 - Successful login")
    @Description("Test Description: Verify if a user will be able to login with a valid credentials")
    public void loginUser1() {

        homePage.goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();

        loginPage
                .verifyLoginPageTitle("Log In")
                .loginWithNewRandomUser();


    }

    @Test(priority = 3, description = "TC017 - Create post")
    @Description("Test Description: Verify that user can create post")
    public void createNewPost() throws InterruptedException {

        String filePath = new File("src/test/resources/img/2.jpg").getAbsolutePath();


        loginPage
                .goToProfilePage();

        profilePage
                .verifyProfilePageTitle("Profile")
                .createNewPost(PostStatus.PUBLIC, "Telerik Academy", "Alpha QA May 2019", filePath);

        profilePage
                .verifyNewPostAppears("Telerik Academy");


    }

    @Test(priority = 4, description = "TC - Connect Friend")
    @Description("Test Description: Verify that user can connect other user")
    public void sendFriendRequest() {


        profilePage
                .goToNewsFeed();

        newsFeed
                .verifyNewsFeedTitle("Title")
                .searchUser("testuser4")
                .goToUsersPage("testuser4");
        newsFeed.connectUser();
        newsFeed.verifyConnectIsSent();

        newsFeed.logOut();
    }

    @Test(priority = 5, description = "TC007 - Successful login")
    @Description("Test Description: Verify if a user will be able to login with a valid credentials")
    public void loginUser2() {

        loginPage
                .loginToIllumni("testuser4", "testpass4")
                .goToProfilePage();

        profilePage
                .verifyProfilePageTitle("Profile");


    }
    @Test(priority = 6, description = "TC007 - Successful login")
    @Description("Test Description: Verify if a user will be able to login with a valid credentials")
    public void acceptFriendRequestUser2() {


        profilePage
                .acceptFriendRequest()
                .logOut();

    }
}
