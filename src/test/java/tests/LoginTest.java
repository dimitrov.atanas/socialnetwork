package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.Listeners.Retry;
import utils.Listeners.TestListener;

@Listeners({TestListener.class})
//@Epic("Regression Tests")
@Feature("Login Tests")


public class LoginTest extends BaseTest{


//    @Test(retryAnalyzer = Retry.class, priority = 1, description = "TC 007 - Successful login")
//    //@Severity(SeverityLevel.CRITICAL)
//    @Description("Test Description: Verify if a user will be able to login with a valid credentials")
//
//    public void successfulLoginTest_ValidUserName_ValidPassword()  {
//
//        homePage
//                .goToHomePage()
//                .verifyHomePageTitle("Home")
//                .goToLoginPage();
//
//        loginPage
//                .verifyLoginPageTitle("Log In")
//                .loginToIllumni("testuser1", "testpass1")
//                .verifyValidLoginConditions("Log Out");
//
//        homePage.logOut();
//
//
//    }
    @Test(retryAnalyzer = Retry.class, priority = 1, description = "TC 008 - Unsuccessful login with empty username and password")
    @Description("Test Description: Verify that a user will not be able to log in with empty username and password")
    public void unsuccessfulLoginTest_EmptyUserName_EmptyPassword()  {

        homePage
                .goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();
        loginPage
                .verifyLoginPageTitle("Log In")
                .loginToIllumni("", "")
                .verifyWrongUsernameConditions()
                .verifyWrongPasswordConditions();

    }
    @Test(priority = 1, description = "TC 009 - Unsuccessful login with non existing username and password")

    @Description("Test Description: Verify that a user will not be able to log in with  non existing username and password")

    public void unsuccessfulLoginTest_InvalidUserName_InvalidPassword() {

        homePage
                .goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();
        loginPage
                .verifyLoginPageTitle("Log In")
                .loginToIllumni("123g", "123g")
                .verifyWrongUsernameConditions()
                .verifyWrongPasswordConditions();
    }
    @Test(priority = 1, description = "TC010 - Unsuccessful login with valid username and wrong password")
    @Description("Test Description: Verify that a user will not be able to log in with valid username and wrong password")
    public void unsuccessfulLoginTest_ValidUserName_WrongPassword() {

        homePage
                .goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();
        loginPage
                .verifyLoginPageTitle("Log In")
                .loginToIllumni("testuser1", "123g")
                .verifyWrongPasswordConditions();
    }
    @Test(priority = 1, description = "TC011 - Unsuccessful login with wrong username and valid password")
    @Description("Test Description: Verify that a user will not be able to log in with wrong username and valid password")
    public void unsuccessfulLoginTest_WrongUserName_ValidPassword() {

        homePage
                .goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();
        loginPage
                .verifyLoginPageTitle("Log In")
                .loginToIllumni("12", "123g")
                .verifyWrongUsernameConditions();
    }
    @Test(priority = 1, description = "TC012 - Unsuccessful login with valid username and empty password")
    @Description("Test Description: Verify that a user will not be able to log in with valid username and empty password")
    public void unsuccessfulLoginTest_ValidUserName_EmptyPassword() {

        homePage
                .goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();
        loginPage
                .verifyLoginPageTitle("Log In")
                .loginToIllumni("testuser1", "")
                .verifyWrongPasswordConditions();
    }
    @Test(priority = 1, description = "TC013 - Unsuccessful login with empty username and valid password")
    @Description("Test Description: Verify that a user will not be able to log in with valid username and empty password")
    public void unsuccessfulLoginTest_EmptyUserName_ValidPassword(){

        homePage
                .goToHomePage()
                .verifyHomePageTitle("Home")
                .goToLoginPage();
        loginPage
                .verifyLoginPageTitle("Log In")
                .loginToIllumni("", "testpass1")
                .verifyWrongUsernameConditions();
    }
}


