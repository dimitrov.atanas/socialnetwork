package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

//import static pages.DriverFactory.getLoginPage;


public class LoginPage extends BasePage {



    public LoginPage(WebDriver driver) {
        super(driver);
    }
    private By usernameId = By.id("username");
    private By passwordId = By.id("password");
    private By loginButtonId = By.id("login-button");
    private By errorMessageUsernameXpath = By.xpath("//*[@id=\"subheader\"]/div/div/div/p[1]/i");
    private By errorMessageEnterUsername = By.xpath("//div[@data-validate=\"Enter Username\"]");
    private By errorMessageEnterPassword = By.xpath("//div[@data-validate=\"Enter Password\"]");


    public LoginPage verifyLoginPageTitle(String title){
        Assert.assertEquals(driver.getTitle(),title);
        return this;
    }
    @Step("Login with username: {0}, password: {1}, for method: {method}")
    public LoginPage loginToIllumni(String username, String password){
        //enter Username/Email
        fillField(usernameId, username);
        //enter Password
        fillField(passwordId,password);
        //click Login Button
        clickElement(loginButtonId);
        return this;
    }

    public LoginPage loginWithNewRandomUser() {
        fillField(usernameId, getUser().getName());
        fillField(passwordId,getUser().getPassword());
        clickElement(loginButtonId);

        return this;
    }


    @Step("Verify wrong login condition {0} ")
    public LoginPage verifyWrongLoginConditions(String expectedText) {
        waitForElementToBeVisible(errorMessageUsernameXpath);
        assertEquals(errorMessageUsernameXpath, expectedText);
        return this;
    }
    //verify Wrong Login Condition Username
    @Step("Verify Wrong Username")
    public LoginPage verifyWrongUsernameConditions(){
        waitForElementToBeVisible(errorMessageEnterUsername);
        assertEquals(errorMessageEnterUsername, "");
        return this;
    }

    //verify Wrong Login Condition Password
    @Step("Verify Wrong Password")
    public LoginPage verifyWrongPasswordConditions(){
        waitForElementToBeVisible(errorMessageEnterPassword);
        assertEquals(errorMessageEnterPassword,"");
        return this;
    }
    //verify Valid Login Condition
    @Step("Verify Valid Login Conditions {0} ")
    public LoginPage verifyValidLoginConditions(String expectedText){
        waitForElementToBeVisible(logOutButton);
        assertEquals(logOutButton,expectedText);
        return this;
    }
    @Step("Go to Profile page")
    public LoginPage goToProfilePage() {
        clickElement(profile);

        return this;
    }

//    private void fillUserName() {
//
//        fillField(userName, LoginPage.user);
//    }
//
//    private void fillPassword() {
//        fillField(password, LoginPage.pass);
//    }
//
//
//    private void loginButtonClick() {
//
//        clickElement(logIn);
//
//    }
//
//    public void login() {
//
//        //driver.get(getLoginPage());
//        fillUserName();
//        fillPassword();
//        loginButtonClick();
//        System.out.println("[INFO] Logged in.");
//    }
//
//    public static String getUser() {
//
//        return user;
//    }


}
