package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.security.SecureRandom;

public class RegistrationPage extends BasePage {

    private By emailField = By.id("email");
    private By usernameField = By.id("username");
    private By passwordField = By.id("password");
    private By registerButton = By.id("register");


    public RegistrationPage(WebDriver driver) {
        super(driver);
    }


    public RegistrationPage registerNewUser(String email, String username, String password) {

        fillField(emailField, email);
        fillField(usernameField, username);
        fillField(passwordField, password);
        clickElement(registerButton);
        return this;
    }

    public RegistrationPage registerNewRandomUser() {

        fillField(emailField, getUser().getEmail());
        fillField(usernameField, getUser().getName());
        fillField(passwordField,getUser().getPassword());
        clickElement(registerButton);
        return this;
    }


    @Step("Verify Registration Page Title")
    public RegistrationPage verifyRegistrationPageTitle(String title){

        Assert.assertEquals(driver.getTitle(),title);
        return this;
    }


    static final String AB = "abcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }




}
