package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pages.enums.PostStatus;


public class NewsFeed extends BasePage {
    private By buttonNewPost = By.xpath("//button[contains(text(),'Post')]");
    private By settings = By.xpath("//a[contains(text(),'Settings')]");
    private By publicPost = By.xpath("//*[@value='public']");
    private By privatePost = By.xpath("//*[@value='private']");
    private By titleField = By.name("title");
    private By descriptionField = By.name("description");
    private By picField = By.name("files");
    private By createPostButton = By.xpath("//*[@type='submit']");
    private By searchUserField = By.id("myInput");
    private By connectButton = By.xpath("//*[contains(text(),' Connect ')]");
    private By disconnectButton = By.xpath("//*[contains(text(),' Disconnect ')]");
    private By connectDisconnectButton = By.id("button-accept-decline");



    public NewsFeed(WebDriver driver) {
        super(driver);
    }
    @Step("Connect User")
    public NewsFeed connectUser() {
        clickElement(connectButton);
        return this;
    }
    @Step("Disconnect User")
    public NewsFeed disconnectUser() {
        clickElement(disconnectButton);
        return this;
    }
    @Step("Search User")
    public NewsFeed searchUser(String user) {
        clearField(searchUserField);
        fillField(searchUserField, user);
        return this;
    }
    @Step(" Go to Users Page")
    public NewsFeed goToUsersPage(String user) {
        clickElement(By.xpath("//*[contains(text(),'" + user + "')]"));
        return this;
    }


    @Step("Verify Connect Request is sent")
    public NewsFeed verifyConnectIsSent() {

        waitForElementToBeVisible(disconnectButton);
        return this;
    }


    @Step("Verify Users Page")
    public NewsFeed verifyUsersPage(String user) {
        waitForElementToBeVisible(By.xpath("//script[contains(text(),'" + user + "')]"));
        return this;
    }
    @Step("Verify News Feed Title")
    public NewsFeed verifyNewsFeedTitle(String title){
        Assert.assertEquals(driver.getTitle(),title);
        return this;
    }
    @Step("Go to Profile page")
    public NewsFeed goToProfilePage(){
        clickElement(profile);
        return this;
    }
}
