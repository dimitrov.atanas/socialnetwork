package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class HomePage extends BasePage {


    private static final String baseUrl = "http://46.10.214.255:8080/";//"http://localhost:8080";//
    private By loginButton = By.xpath("//*[@id=\"navbar-collapse-main\"]/ul/li[2]/a");

    public HomePage(WebDriver driver){
        super(driver);

    }
    @Step("Go to Home Page")
    public HomePage goToHomePage() {
        driver.get(baseUrl);
        return this;
    }
    @Step("Verify register Page Title")
    public HomePage goToRegistrationPage() {
        clickElement(getStartedBtn);
        return this;
    }

    @Step("Verify Home Page Title")
    public HomePage verifyHomePageTitle(String title){

        Assert.assertEquals(driver.getTitle(),title);
        return this;
    }
    @Step("Go to Login Page")
    public HomePage goToLoginPage() {
        clickElement(loginButton);
        return this;
    }

    @Step("Go to Profile page")
    public HomePage goToProfilePage() {
        clickElement(profile);

        return this;
    }

}

