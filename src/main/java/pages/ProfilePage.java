package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pages.enums.PostStatus;

import java.util.List;


public class ProfilePage extends BasePage {

    //button[contains(text(),'Post')]

    private By buttonNewPost = By.xpath("//button[contains(text(),'New')]");
    private By settings = By.xpath("//a[contains(text(),'Settings')]");
    private By publicPost = By.xpath("//*[@value='public']");
    private By privatePost = By.xpath("//*[@value='private']");
    private By titleField = By.name("title");
    private By descriptionField = By.name("description");
    private By picField = By.id("file-input-home");
    private By createPostButton = By.xpath("//*[@type='submit']");
    private By personalPost = By.xpath("//*[@class='w3-container w3-card w3-white w3-margin fadeIn']");
    private By acceptFriendRequestButton = By.id("accept");


    public ProfilePage(WebDriver driver) {
        super(driver);

    }



    void clickAccountSettings() {
        clickElement(settings);

    }

    @Step("Accept Friend Request")
    public ProfilePage acceptFriendRequest() {

        clickElement(acceptFriendRequestButton);
        return this;
    }

    @Step("Create New Post")
    public void createNewPost(PostStatus postStatus, String title, String description, String picture) throws InterruptedException {
        Thread.sleep(500);

        clickElement(buttonNewPost);

        Thread.sleep(500);

        switch (postStatus) {
            case PRIVATE:

                clickElement(privatePost);
                break;
            default:
                clickElement(publicPost);
                break;
        }

        fillField(titleField, title);


        fillField(descriptionField, description);


        fillField(picField, picture);


        clickElement(createPostButton);
        Thread.sleep(500);


    }

    @Step("Go to News Feed")
    public ProfilePage goToNewsFeed() {
        clickElement(newsFeedButton);
        return this;
    }

    @Step("Verify Profile Page Title")
    public ProfilePage verifyProfilePageTitle(String title) {
        Assert.assertEquals(driver.getTitle(), title);
        return this;
    }

    @Step("Verify Request is accepted")
    public ProfilePage verifyRequestIsAccepted() {

        waitForElementToGoInvisible(acceptFriendRequestButton);
        return this;
    }

    public ProfilePage verifyNewPostAppears(String title) {

        Assert.assertTrue(driver.getPageSource().contains(title));
        return this;
    }
}
