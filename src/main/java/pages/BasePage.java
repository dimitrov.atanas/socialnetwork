package pages;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.User;

import java.security.SecureRandom;

class BasePage extends NavBar {

    By getStartedBtn = By.xpath("//*[@class='getStartedButton']");
    By logOutButton = By.xpath("//a[contains(text(),'Out')]");
    By newsFeedButton = By.xpath("//*[contains(text(),'News')]");
    private static final User user = new User();


    public User getUser() {
        return user;
    }

    WebDriver driver;
    private WebDriverWait wait;



    BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }


    void clickElement(By by) {

        waitForElementToBeVisible(by);
        driver.findElement(by).click();
    }

    boolean  waitForElementToGoInvisible(By by) {

       return new WebDriverWait(driver, 3)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }


    void waitForElementToBeVisible(By by) {

        WebElement element = (new WebDriverWait(driver, 15))
                .until(ExpectedConditions.presenceOfElementLocated(by));


    }

    void fillField(By by, String text) {
        waitForElementToBeVisible(by);
        driver.findElement(by).sendKeys(text);
    }

    void clearField(By by) {
        waitForElementToBeVisible(by);
        driver.findElement(by).clear();
    }


    private String readText(By elementBy) {
        waitForElementToBeVisible(elementBy);
        return driver.findElement(elementBy).getText();
    }

    void assertEquals(By elementBy, String expectedText) {
        waitForElementToBeVisible(elementBy);
        Assert.assertEquals(readText(elementBy), expectedText);
    }

    public void logOut() {
        clickElement(logOutButton);
    }

    static final String AB = "abcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }
}
