## Windows

For Windows, Allure is available from the Scoop commandline-installer

Scoop is a command-line installer for Windows.

Run below command from your PowerShell to install scoop to its default location (C:\Users\\scoop)

Command line for allure

One of the easiest way to start PowerShell in Windows, is using Run window. Press Win + R keys on your keyboard, then type powershell and press Enter or click OK.

The command should be:<br>
 *iex (new-object net.webclient).downloadstring('https://get.scoop.sh')*

 If you get an error, you might have to set the execution policy (i.e. enable Powershell)with <br>*Set-ExecutionPolicy RemoteSigned -scope CurrentUser*
 To install Allure, using Scoop, run the below command<br>
*scoop install allure*

## Linux
For debian-based repositories a Personal Package Archive (PPA) is provided by qameta:

*sudo apt-add-repository ppa:qameta/allure<br>
sudo apt-get update <br>
sudo apt-get install allure*

## Mac
For Mas OS, automated installation is available via Homebrew<br>
*brew install allure*




