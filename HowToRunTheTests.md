## I.	Prerequisites

1.	Install JDK 8+
2.	Install Maven
3.	Install Git
4.	Install Allure
5.	Install Newman

## II.	How to run the tests

1.	Clone or download the following repository https://gitlab.com/dimitrov.atanas/socialnetwork
2.	For Windows – Open folder Run_Test_Windows
3.	Select test suite and double click it, or run as administrator
4.	For Ubuntu – Open folder linuxBash
5.	Select test suite and double click it, or run as administrator
